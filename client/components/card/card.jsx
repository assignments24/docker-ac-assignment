import './card.css';

const Card = ({monster}) =>{ 

    const onClickHandle = () => {
      console.log("desu bom")
    }

    const {name , id , email } = monster;
    return (
      <div className='card-container' key={id} onClick={() => onClickHandle()}>
        <img alt={`monster ${name}` }src={`https://robohash.org/${id}?set=set2?size=180x180`} />
        <h2>{name}</h2>
        <p>{email}</p>
      </div>
    )
 
}

export default Card;
