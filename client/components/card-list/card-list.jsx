import Card from '../card/card';
import './card-list.css';

const CardList = ({monsters , onClickHandle}) => (
  
  <div className="card-list">
    {monsters?.map((monster) => {
    return ( 
      <Card monster={monster} onClickHandle={onClickHandle()}/>
    )})}
  </div>
)  


export default CardList;
