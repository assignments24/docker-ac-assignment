import { useEffect, useState } from 'react';
import './App.css';
import axios from 'axios';
import CardList from './components/card-list/card-list';
import SearchBox from './components/search-box/search-box';


const App = () => {
  
  console.log("render")

  const [searchField ,  setSearchField ] = useState('')
  const [monsters ,  setMonsters ] = useState([])

  useEffect( () => {
    axios('https://jsonplaceholder.typicode.com/users')
          .then((response) => response.data)
          .then((users) => setMonsters(users))
  },[])

  const handleChange = (event) => { 
    const stringField = event.target.value.toLocaleLowerCase();
    setSearchField(stringField)
  }

  const array = monsters.filter((monster) => monster.name.toLocaleLowerCase().includes(searchField));

  const onHandleClick = (monster) => {
    const book = {
      id: monster.id,
      book_name : monster.name
    }
    axios.post('/api/insert', book)
        .then(() => { alert('success post') })
    console.log(monster)
  }

  return (
    <div className="App">
      <h1>Monster Roledex </h1>
      <SearchBox onChangeHandler={handleChange}/>
      
      <CardList monsters={array} onHandleClick={onHandleClick}/>
    </div>
  );
}

export default App;
