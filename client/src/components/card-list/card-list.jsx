import Card from '../card/card';
import './card-list.css';

const CardList = ({monsters , onHandleClick}) => (
  
  <div className="card-list">
    {monsters?.map((monster) => {
    return ( 
      <Card monster={monster} onHandleClick={onHandleClick} key={monster.id} />
    )})}
  </div>
)  


export default CardList;
